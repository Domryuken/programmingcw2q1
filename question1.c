#include <stdio.h>
#include <stdlib.h>
#include <math.h>




struct dataStruct
{
    int size;
    int *values;
    int numberCount[10];
};


struct dataStruct readIn(char*file) 
{
    struct dataStruct Data;    
    FILE * input = fopen(file, "r");
    if (input == NULL)
    {
        printf("Error: File empty");
        exit(EXIT_FAILURE);
    }
    fscanf(input, "%d", &Data.size);
    Data.values = (int*)malloc((Data.size)*sizeof(int));
    int i;
    for (i = 0; i < Data.size; i++)
    {
        fscanf(input, "%d", &Data.values[i]);
    }
    fclose(input);
    return Data;
}


void writeOut(struct dataStruct *Data,double mean, int mode, int median, double stdDev)
{
    FILE * output = fopen("output.txt", "w");
    fprintf(output,"\r\nMean: %f",mean);
    fprintf(output,"\r\nMode: %d",mode);
    fprintf(output,"\r\nMedian: %d",median);
    fprintf(output,"\r\nStandard Deviation: %f",stdDev);
    fprintf(output,"\r\n\r\nHistogram");
    int i;
    for(i=0;i<10;i++)
    {
        fprintf(output,"\r\n%2d : ",(i+1));
        int j;
        for(j=0;j<(*Data).numberCount[i];j++)
        {
            fprintf(output,"*");
        }
    }
    fclose(output);
}


void countNumbers(struct dataStruct *Data)
{
    int i;
    for(i=0;i<10;i++)
    {
        (*Data).numberCount[i] = 0;
    }
    for(i=0;i<(*Data).size;i++)
    {
        (*Data).numberCount[(*Data).values[i]-1]++;
    }
}


double calculateMean(struct dataStruct *Data)
{
    int i;
    int sum = 0;
    for(i=0;i<(*Data).size;i++)
    {
        sum = sum + (*Data).values[i];
    }
    double mean = (double)sum/(*Data).size;
    return mean;
}


int calculateMode(struct dataStruct *Data)
{
    int i;
    int mode = 0;
    int total = 0;
    for(i=0;i<10;i++)
    {
        if((*Data).numberCount[i]>total)
        {
            total = (*Data).numberCount[i];
            mode = i+1;
        }
    }
    return mode;
}


int calculateMedian(struct dataStruct *Data)
{
    int median=0;
    int total = 0;
    while(total<(*Data).size/2)
    {
        total = total + (*Data).numberCount[median];
        median++;
    }
    return median;
}


double calculateStdDev(struct dataStruct *Data, double mean)
{
    int i;
    double stdDeviation;
    double variance = 0;
    for(i=0;i<(*Data).size;i++)
    {
        variance+=((*Data).values[i]-mean)*((*Data).values[i]-mean);
    }
    stdDeviation = sqrt((double)variance/(*Data).size);
    return stdDeviation;
}


int main(int argc, char** argv)
{
    struct dataStruct Data = readIn("dataset2.txt");
    struct dataStruct *DataStr = &Data;
    countNumbers(DataStr);
    double mean = calculateMean(DataStr);
    int mode = calculateMode(DataStr);
    int median = calculateMedian(DataStr);
    double stdDev = calculateStdDev(DataStr,mean);
    writeOut(DataStr,mean,mode,median,stdDev);    
    return (EXIT_SUCCESS);
}